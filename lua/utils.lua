local M={}


function M.setupMD()
    vim.opt.textwidth=100
    vim.opt.updatetime=1
    vim.opt.spelllang=[[en_gb]]
    -- vim.opt.spellfile=[[/usr/share/hunspell/en_GB.dic]]
    if vim.fn.has('pandoc') then
       vim.opt.makeprg=[[pandoc -s % --pdf-engine=xelatex -o %:r.pdf & disown]] 
    end
end



return M
