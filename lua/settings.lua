
vim.opt.relativenumber=true

--
-- tab Configuration
--
vim.opt.tabstop=8
vim.opt.shiftwidth=4
vim.opt.softtabstop=0
vim.opt.expandtab=true
vim.opt.smartindent=true
vim.opt.smarttab=true

--
-- folds Configuration
--
vim.opt.foldenable=true
vim.opt.foldmethod='indent'
vim.opt.foldlevelstart=0
vim.opt.foldnestmax=10
vim.opt.foldlevel=0

--
-- Search Configuration in a file
--
-- set ignorecase is set just for fortran files
vim.opt.incsearch=true   -- search as characters are entered
vim.opt.hlsearch=true    -- highlight matches
vim.opt.smartcase=true   -- uppercase causes case-sensitive search
vim.opt.wrapscan=true    -- searches wrap back to the top of file
--
--
-- netrw Configuration
--
--vim.g.loaded_netrw=1
vim.g.netrw_preview=1           -- when preview is activated with p open in a vertical split, 0 for horizontal
vim.g.netrw_browse_split=4
vim.g.netrw_alto=0
    --g:netrw_preview g:netrw_alto result
    --         0             0     |:aboveleft|
    --         0             1     |:belowright|
    --         1             0     |:topleft|
    --         1             1     |:botright|
vim.g.netrw_banner=0
vim.g.netrw_liststyle = 3
vim.g.netrw_winsize   = 80
vim.g.netrw_bufsettings ='rnu'
vim.g.netrw_list_hide= '.git/,.cvs/'
--
-- UI Configuration
--
vim.opt.cursorline=true          -- highlight current line
vim.opt.number=true              -- show line numbers
vim.opt.relativenumber=true      -- Set relative number depending on the cursor position
                        -- In case where number is not set, the editor will
                        -- show just the relative number of line, otherwise it
                        -- will use a hybrid where the cursor shows the global
                        -- number
vim.opt.showcmd=true             -- show command in bottom bar
vim.opt.wildmenu=true            -- visual autocomplete for command menu
vim.opt.lazyredraw=true          -- redraw only when we need to.
vim.opt.showmatch=true           -- highlight matching [{()}]
vim.opt.ruler=true               -- Show line, column number, and relative position within a file in the status line
vim.opt.fileencoding='utf-8'
vim.opt.fileformat='unix'
vim.opt.fileformats='unix'
vim.opt.ff='unix'
--vim.opt.statusline+=%#warningmsg#
--vim.opt.statusline+=%*
vim.opt.laststatus=2        -- show the status line
vim.opt.showmode=false        -- disable the show mode because it is appearing in the statusline defined above
vim.opt.shortmess='atToO'
vim.opt.backspace={indent,eol,start}

--
-- Window spliting
--
vim.opt.splitright=true
vim.opt.splitbelow=true

vim.cmd[[colorscheme gruvbox]]
vim.cmd[[hi Normal guibg=NONE ctermbg=NONE]] --transparent background

if vim.fn.has('ripgrep') then
   vim.opt.grepprg=[[rg --vimgrep --hidden --glob '!.git' --no-heading --follow $* ]]
   --vim.o.grepformat = add('%f:%l:%c:%m', vim.o.grepformat)
end

