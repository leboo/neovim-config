-----------------------------------------------------------
-- Autocomplete configuration file
-----------------------------------------------------------

-- Plugin: nvim-cmp
-- https://github.com/hrsh7th/nvim-cmpa


local cmp = require 'cmp'
local luasnip = require 'luasnip'

cmp.setup {
    -- load snippet support
    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end,
    },

    -- completion settings
    completion = {
        --completeopt = 'menu,menuone,noselect'
        keyword_length = 2
    },

    -- key mapping
    mapping = {
        ['<C-n>'] = cmp.mapping.select_next_item(),
        ['<C-p>'] = cmp.mapping.select_prev_item(),
        ['<C-d>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<C-e>'] = cmp.mapping.close(),
        ['<CR>'] = cmp.mapping.confirm {
            behavior = cmp.ConfirmBehavior.Replace,
            select = true,
        },

        -- Tab mapping
        ['<Tab>'] = function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            elseif luasnip.expand_or_jumpable() then
                luasnip.expand_or_jump()
            else
                fallback()
            end
        end,
        ['<S-Tab>'] = function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            elseif luasnip.jumpable(-1) then
                luasnip.jump(-1)
            else
                fallback()
            end
        end
    },

    -- format
    
     
    -- load sources, see: https://github.com/topics/nvim-cmp
    sources = {                                -- order matter below for filling autocompletion 
        { name = 'nvim_lsp' },                 -- language server protocol 
        { name = 'luasnip' },                  -- for snippets 
        { name = 'nvim_lua' },                 -- for nvim lua function
        { name = 'path' },                     -- for path completion 
        { name = 'buffer', keyword_length=4 }, -- for buffer word completion 
    },
} 
