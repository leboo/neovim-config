local packer= require([[packer]])

return packer.startup(function()
    -- Packercan manage itself
    use 'wbthomason/packer.nvim'

    -- status line configuration
    use 'itchyny/lightline.vim'

    -- adding onedark colorscheme as optional
    use {'joshdick/onedark.vim', opt=true}

    -- default color scheme for the current configuration 
    use {'morhetz/gruvbox'}

    -- plugin enabling easy un/comment
    use 'scrooloose/nerdcommenter'

    -- auto insert, delete for [] () "" ''
    use 'jiangmiao/auto-pairs'

    -- plugin for printing tags in a buffer 
    -- making easier to reach variable/function .... 
    -- in a large file 
    use 'majutsushi/tagbar'

    use 'junegunn/fzf'

    -- lsp for a friendly configuration
    use 'neovim/nvim-lspconfig'

    -- autocomplete plugin 
    use {
        'hrsh7th/nvim-cmp',
        requires = {
            'hrsh7th/cmp-nvim-lsp',
            'hrsh7th/cmp-path',
            'hrsh7th/cmp-buffer',
            'hrsh7th/cmp-nvim-lua',
            'saadparwaiz1/cmp_luasnip',
            'L3MON4D3/LuaSnip',
        },
    }
end)
