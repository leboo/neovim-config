--
-- configuration for tagbar plugin
--

vim.g['tagbar_compact']=true   -- remove help message
vim.g['tagbar_width']=35        -- in characters
vim.g['tagbar_autofocus']=true -- cursor jump into tagbar window when opne
