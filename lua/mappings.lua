local map = vim.api.nvim_set_keymap
local default_opts = { noremap = true, silent = true }

vim.g.mapleader=' '
vim.g.maplocalleader='\\'
--
-- mapping jj to escape
--
map('i', 'jj', '<Esc>', default_opts)
--
-- netrw toggle
--
map('n','<Leader>x','<cmd>Lexplore<CR>', default_opts)
--
-- mapping to navigate between windows in
-- normal and terminal mode
--
map('n','<C-l>', '<C-w>l', default_opts)
map('n','<C-h>', '<C-w>h', default_opts)
map('n','<C-j>', '<C-w>j', default_opts)
map('n','<C-k>', '<C-w>k', default_opts)
map('t','<C-l>', '<C-w>l', default_opts)
map('t','<C-h>', '<C-w>h', default_opts)
map('t','<C-j>', '<C-w>j', default_opts)
map('t','<C-k>', '<C-w>k', default_opts)

--
-- quick splitting mapping
--
map('n','<Leader>s', '<cmd>split<CR>', default_opts)
map('n','<Leader>v', '<cmd>vertical split<CR>', default_opts)
--
-- mapping  tagbar
--
map('n', '<F8>', '<cmd>TagbarToggle<CR>', default_opts)

--
-- fzf mappings
--

map('n','<Leader>ff','<cmd>FZF<CR>',default_opts)
