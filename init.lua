
-- ----------------------------------
-- 	Importing lua modules
-- ----------------------------------
require ('settings')
require ('mappings')
require ('plugins/packer')
require ('plugins/lspconfig')
require ('plugins/nvim-cmp')
require ('plugins/tagbar')
